package com.kolchak.model;

public interface BeanValidator {
    void validate();
}
