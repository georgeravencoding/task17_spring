package com.kolchak;

//import com.kolchak.view.MyView;
import com.kolchak.controller.BeanConfigOne;
import com.kolchak.model.*;
import org.apache.logging.log4j.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigOne.class);
        BeanB beanB = context.getBean(BeanB.class);
        System.out.println(beanB);

        BeanC beanC = context.getBean(BeanC.class);
        System.out.println(beanC);

        BeanD beanD = context.getBean(BeanD.class);
        System.out.println(beanD);

        BeanA beanA = context.getBean(BeanA.class);
        System.out.println(beanA);

        BeanE beanE = context.getBean(BeanE.class);
        System.out.println(beanE);

        BeanF beanF = context.getBean(BeanF.class);
        System.out.println(beanF);


    }
}
