package com.kolchak.model;

import org.springframework.beans.factory.annotation.Value;

public class BeanC implements BeanValidator {
    @Value("${BeanC.name}")
    private String name;
    @Value("${BeanC.value}")
    private int value;

    public BeanC() {
        System.out.println("BeanC initialized");
    }

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void validate() {
        System.out.println("BeanC: Bean Validation");
    }

    public void initMethod() {
        System.out.println("BeanC: Initialization");
    }

    public void destroyMethod() {
        System.out.println("BeanC: Destruction");
    }
}
