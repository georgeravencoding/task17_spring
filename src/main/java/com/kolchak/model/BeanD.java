package com.kolchak.model;

import org.springframework.beans.factory.annotation.Value;

public class BeanD implements BeanValidator {
    @Value("${BeanD.name}")
    private String name;
    @Value("${BeanD.value}")
    private int value;

    public BeanD() {
        System.out.println("BeanD initialized");
    }

    public BeanD(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void validate() {
        System.out.println("BeanD: Bean Validation");
    }

    public void initMethod() {
        System.out.println("BeanD: Initialization");
    }

    public void destroyMethod() {
        System.out.println("BeanD: Destruction");
    }
}
