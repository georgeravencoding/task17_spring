package com.kolchak.model;

import org.springframework.beans.factory.annotation.Value;

public class BeanB implements BeanValidator {
    @Value("${BeanB.name}")
    private String name;
    @Value("${BeanB.value}")
    private int value;

    public BeanB() {
        System.out.println("BeanB initialized");
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void validate() {
        System.out.println("BeanB: Bean Validation");
    }

    public void initMethod() {
        System.out.println("BeanB: Initialization");
    }

    public void destroyMethod() {
        System.out.println("BeanB: Destruction");
    }
}
