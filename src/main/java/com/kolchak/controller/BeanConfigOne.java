package com.kolchak.controller;

import com.kolchak.model.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("my.properties")
@ComponentScan("com.kolchak.model")
@Import(BeanConfigTwo.class)
public class BeanConfigOne {
    @Bean("beanA")
    BeanA getBeanA1(BeanB beanB, BeanC beanC) {
        return new BeanA(beanB.getName(), beanC.getValue());
    }

    @Bean("beanA")
    BeanA getBean2(BeanB beanB, BeanD beanD) {
        return new BeanA(beanB.getName(), beanD.getValue());
    }

    @Bean("beanA")
    BeanA getBeanA3(BeanC beanC, BeanD beanD) {
        return new BeanA(beanC.getName(), beanD.getValue());
    }

    @Bean("beanE")
    BeanE getBeanE1(BeanA getBeanA1) {
        return new BeanE(getBeanA1.getName(), getBeanA1.getValue());
    }

    @Bean("beanE")
    BeanE getBeanE2(BeanA getBeanA2) {
        return new BeanE(getBeanA2.getName(), getBeanA2.getValue());
    }

    @Bean("beanE")
    BeanE getBeanE3(BeanA getBeanA3) {
        return new BeanE(getBeanA3.getName(), getBeanA3.getValue());
    }
}
