package com.kolchak.model;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private String name;
    private int value;

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void validate() {
        System.out.println("BeanA: Bean Validation");
        if (name == null) {

        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("BeanA: After properties set");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("BeanA: Destroy BeanA");
    }
}
