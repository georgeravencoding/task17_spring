package com.kolchak.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private String name;
    private int value;

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void validate() {
        System.out.println("BeanE: Bean Validation");
    }

    @PostConstruct
    public void annoInitMethod() {
        System.out.println("BeanE: Inside @PostConstruct-menthod");
    }

    @PreDestroy
    public void annoDestroyMethod() {
        System.out.println("BeanE: Inside @PreDestroy-method");
    }
}
