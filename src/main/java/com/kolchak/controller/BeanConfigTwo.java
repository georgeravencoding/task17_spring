package com.kolchak.controller;

import com.kolchak.model.BeanB;
import com.kolchak.model.BeanC;
import com.kolchak.model.BeanD;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class BeanConfigTwo {
    @Bean("beanB")
    @DependsOn(value = {"beanC", "beanD"})
    BeanB getBeanB() {
        return new BeanB();
    }

    @Bean("beanC")
    @DependsOn(value = "beanD")
    BeanC getBeanC() {
        return new BeanC();
    }

    @Bean("beanD")
    BeanD getBeanD() {
        return new BeanD();
    }
}
